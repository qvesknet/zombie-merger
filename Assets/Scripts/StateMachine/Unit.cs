﻿using System.Collections;
using System.Collections.Generic;
using CoreMechanics;
using StateMachine.States;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

namespace StateMachine
{
    public class Unit : MonoBehaviour
    {
        [HideInInspector] public global::StateMachine.StateMachine stateMachine = new global::StateMachine.StateMachine();
        [HideInInspector] public IdleState                 idle;
        [HideInInspector] public MoveState                 move;
        [HideInInspector] public AttackState               attack;
        [HideInInspector] public WinState                  win;
        [HideInInspector] public DeathState                death;

        private void Awake()
        {
            idle   = new IdleState(this);
            move   = new MoveState(this);
            attack = new AttackState(this);
            win    = new WinState(this);
            death  = new DeathState(this);
        }

        // components
        [HideInInspector] public Animator         animator;
        [HideInInspector] public TargetController targetController;
        [HideInInspector] public NavMeshAgent     agent;
    
        // other fields
        [HideInInspector] public GameObject     target;
        [SerializeField]         LayerMask      targetLayerMask;
        public                   int            damage      = 2;
        public                   float          minDistance = 1f;
        void Start()
        {
            animator         = GetComponentInChildren<Animator>();
            agent            = GetComponent<NavMeshAgent>();
            targetController = GetComponent<TargetController>();

            stateMachine.ChangeState(idle);

        }
        void Update()
        {
            stateMachine.Update();
        }

        public int CountTeamEnemies()
        {
            return targetController.AliveTargets;
        }

        public Vector3 Destination()
        {
            var position = target.transform.position;
            return new Vector3(position.x,
                               transform.position.y,
                               position.z);
        }
        public GameObject ChooseTarget()
        {
            targetController.TargetLayerMask = targetLayerMask;
            return targetController.SearchTarget(100f);
        }
        public float CheckDistance()
        {
            return Vector3.Distance(transform.position, target.transform.position);
        }
        public void MakeDamage()
        {
            target.GetComponent<HealthComponent>().TakeDamage(damage);
        }


        public void DestroySelf()
        {
            Destroy(gameObject);
        }
    }
}
