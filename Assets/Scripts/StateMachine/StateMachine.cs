﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    public interface IState
    {
        void Enter();
        void Execute();
        void Exit();
    }
    public class StateMachine
    {
        IState        currentState;
        public IState CurrentState => this.currentState;

        public void ChangeState(IState newState)
        {
            currentState?.Exit();

            currentState = newState;
            currentState.Enter();
        }

        public void Update()
        {
            if (currentState != null) currentState.Execute();
        }
    }
    
}