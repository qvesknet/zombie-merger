﻿using UnityEngine;

namespace StateMachine.States
{
    public class IdleState : IState
    {
        private Avatar avatar;
        
        Unit owner;
        public IdleState(Unit owner)
        {
            this.owner = owner;
        }

        public void Enter()
        {
            avatar = owner.animator.avatar;
            owner.animator.avatar = null;
            owner.animator.avatar = avatar;
            owner.animator.SetFloat("Idle", 1);
            owner.target = owner.ChooseTarget();
        }

        public void Execute()
        {
            // nothing needed
        }

        public void Exit()
        {
            // nothing needed
        }
    }
}
