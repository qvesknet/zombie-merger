﻿namespace StateMachine.States
{
    public class WinState : IState
    {
        Unit owner;
        public WinState(Unit owner)
        {
            this.owner = owner;
        }

        public void Enter()
        {
            owner.agent.isStopped = true;
            owner.animator.SetFloat("Win",    1);
            owner.animator.SetFloat("Attack", 0);
        }

        public void Execute()
        {
        }

        public void Exit()
        {
        }
    }
}
