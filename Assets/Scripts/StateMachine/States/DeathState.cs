﻿using UnityEngine;

namespace StateMachine.States
{
    public class DeathState : IState
    {
        Unit  owner;
        float timer = 2f;
        public DeathState(Unit owner)
        {
            this.owner = owner;
        }
        public void Enter()
        {
            owner.animator.SetFloat("Attack", 0);
            owner.animator.SetBool("Dead", true);
        }

        public void Execute()
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                owner.DestroySelf();
            }
        }

        public void Exit()
        {
        }
    }
}
