﻿using CoreMechanics;
using UnityEngine;

namespace StateMachine.States
{
    public class AttackState : IState
    {
        Unit owner;
        public AttackState(Unit owner)
        {
            this.owner = owner;
        }

        float timeBetweenAttack;
        float timerDefault = 1f;
        public void Enter()
        {
            owner.agent.isStopped = true;
            owner.animator.SetFloat("Attack", 1);
            timeBetweenAttack = timerDefault;
        }

        public void Execute()
        {
            if (owner.CountTeamEnemies() <= 0)
            {
                owner.stateMachine.ChangeState(owner.win);
            }

            timeBetweenAttack -= Time.deltaTime;
            if (owner.target != null && owner.target.GetComponent<HealthComponent>().IsDead())
            {
                owner.target = null;
            }

            if (owner.target == null)
            {
                owner.animator.SetFloat("Attack", 0);
                owner.stateMachine.ChangeState(owner.move);
            }

            else
            {
                owner.transform.LookAt(owner.Destination());
                if(timeBetweenAttack <= 0)
                {
                    owner.MakeDamage();
                    timeBetweenAttack = timerDefault;
                }
            }

        }

        public void Exit()
        {

        }
    }
}
