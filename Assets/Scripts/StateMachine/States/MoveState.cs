﻿using CoreMechanics;

namespace StateMachine.States
{
    public class MoveState : IState
    {
        Unit owner;
        public MoveState(Unit owner)
        {
            this.owner = owner;
        }

        public void Enter()
        {
            owner.agent.enabled = true;
            owner.target        = owner.ChooseTarget();
            owner.animator.SetFloat("Move", 1);
        }

        public void Execute()
        {
            if (owner.CountTeamEnemies() <= 0)
            {
                owner.stateMachine.ChangeState(owner.win);
            }

            if (owner.target != null && owner.target.GetComponent<HealthComponent>().IsDead())
            {
                owner.target = null;
            }

            if (owner.target != null && owner.CheckDistance() > owner.minDistance)
            {
                owner.transform.LookAt(owner.Destination());
                owner.agent.isStopped = false;
                owner.agent.SetDestination(owner.Destination());
            }
            else if(owner.target != null && owner.CheckDistance() < owner.minDistance)
            {

                owner.stateMachine.ChangeState(owner.attack);
            }
            else
            {
                owner.target = owner.ChooseTarget();
            }
        }

        public void Exit()
        {
        }
    }
}
