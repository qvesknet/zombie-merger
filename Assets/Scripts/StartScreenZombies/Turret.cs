﻿using System.Collections;
using Patik.CodeArchitecture.Systems;
using UnityEngine;

namespace StartScreenZombies
{
    public class Turret : MonoBehaviour
    {
        [SerializeField] LayerMask      enemyLayerMask;
        [SerializeField] ParticleSystem gunOne;
        [SerializeField] ParticleSystem gunTwo;
        Zombie                          zombie     = null;
        bool                            isShooting = false;
        [SerializeField] BloodPool      bloodPool;

        private void Update()
        {
            if (zombie == null)
            {
                zombie = SearchForZombies();
                if (zombie != null)
                    LookAtZombie();
            }
        }

        private Zombie SearchForZombies()
        {
            var colliders = Physics.OverlapSphere(transform.position, 8f, enemyLayerMask);

            GameObject target = null;
            if (colliders.Length > 0)
            {
                foreach (var collider in colliders)
                {
                    float dist = Vector3.Distance(transform.position, collider.transform.position);
                    if (dist < 8)
                    {
                        target = collider.gameObject;
                    }
                }
            }

            return target != null ? target.GetComponent<Zombie>() : null;
        }

        private void LookAtZombie()
        {
            var zombieTransform = zombie.transform;
            var position        = zombieTransform.position;
            var zombiePos       = new Vector3(position.x, transform.position.y, position.z);
            transform.LookAt(zombiePos);
            if (isShooting == false)
            {
                StartCoroutine(ShootAtZombies());
            }
        }

        IEnumerator ShootAtZombies()
        {
            isShooting = true;
            gunOne.gameObject.SetActive(true);
            gunTwo.gameObject.SetActive(true);
            Vector3 bloodPos = Vector3.zero;
            while (zombie != null)
            {
                yield return new WaitForSeconds(0.15f);
                if (!zombie.TakeDamage(1))
                {
                    bloodPos = zombie.transform.position;
                    gunOne.gameObject.SetActive(false);
                    gunTwo.gameObject.SetActive(false);
                    break;
                }
            }
            
            var bloodPoolClone = BehaviourPool.Instantiate(bloodPool);
            bloodPoolClone.transform.position = new Vector3(bloodPos.x, 0.01f, bloodPos.z);
            zombie.KillZombie();
            zombie     = null;
            isShooting = false;
            StopCoroutine(ShootAtZombies());
        }
    }
}