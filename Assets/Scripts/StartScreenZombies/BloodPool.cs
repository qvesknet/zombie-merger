﻿using System.Collections;
using Patik.CodeArchitecture.Systems;
using UnityEngine;

namespace StartScreenZombies
{
    public class BloodPool : MonoBehaviour, IPoolableResetableBehaviour<BloodPool>
    {
        Material material;
        Color    color;
        private void Awake()
        {
            material = GetComponent<Renderer>().material;
            color    = material.color;
        }
        public void OnRestart()
        {
            material.color = color;
            StartCoroutine(FadeTo(0.5f, 0.5f));
        }


        public void ResetToPrefab(BloodPool Prefab)
        {

        }

        IEnumerator FadeTo(float aValue, float aTime)
        {
            float alpha = material.color.a;
            yield return new WaitForSeconds(1f);
            for (float t = 0.0f; t < 1f; t += Time.deltaTime / aTime)
            {
                Color newColor = new Color(color.r, color.g, color.b, Mathf.Lerp(alpha, aValue, t));
                material.color = newColor;
                yield return null;
            }
            this.DestroyPooled();
        }
    }
}
