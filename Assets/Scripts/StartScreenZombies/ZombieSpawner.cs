﻿using System;
using System.Collections;
using Patik.CodeArchitecture.Systems;
using UnityEngine;
using Random = UnityEngine.Random;

namespace StartScreenZombies
{
    public class ZombieSpawner : MonoBehaviour
    {
        public static ZombieSpawner Instance;
        private void Awake()
        {
            Instance = this;
        }

        [SerializeField] Zombie     zombie;
        public           GameObject destination;

        [SerializeField] float minX;
        [SerializeField] float maxX;
        [SerializeField] float minZ;
        [SerializeField] float maxZ;
        [SerializeField] float timeBetweenSpawn = 1;
        void Start()
        {
            StartCoroutine(SpawnZombies());
        }

        IEnumerator SpawnZombies()
        {
            yield return  new  WaitForSeconds(1f);
            while(true)
            {
                var randomX     = Random.Range(minX, maxX);
                var randomZ     = Random.Range(minZ, maxZ);
                var zombieClone = BehaviourPool.Instantiate(zombie);
                zombieClone.transform.position = new Vector3(randomX, 1, randomZ);
                yield return new WaitForSeconds(timeBetweenSpawn);
            }
        }
    }
}
