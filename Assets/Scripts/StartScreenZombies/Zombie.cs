﻿using System.Collections;
using CoreMechanics;
using Patik.CodeArchitecture.Systems;
using UnityEngine;
using UnityEngine.AI;

namespace StartScreenZombies
{
    public class Zombie : MonoBehaviour, IPoolableResetableBehaviour<Zombie>
    {
        [SerializeField] int            health = 10;
        NavMeshAgent                    navMeshAgent;
        Animator                        anim;
        private CapsuleCollider         CapsuleCollider;
        Vector3                         initialPosition;
        HealthComponent                 healthComponent;
        [SerializeField] ParticleSystem blood;
    
        private void Awake()
        {
            healthComponent = GetComponent<HealthComponent>();
            healthComponent.SetHealth(health);
        }
        public void OnRestart()
        {
            initialPosition = transform.position;
            healthComponent.ResetHealth();
            navMeshAgent            = GetComponent<NavMeshAgent>();
            navMeshAgent.enabled    = true;
            navMeshAgent.isStopped  = false;
            anim                    = GetComponent<Animator>();
            CapsuleCollider         = GetComponent<CapsuleCollider>();
            CapsuleCollider.enabled = true;
            navMeshAgent.SetDestination(ZombieSpawner.Instance.destination.transform.position);
            anim.SetBool("Dead", false);
            anim.SetFloat("Move", 1);
        }

        public void ResetToPrefab(Zombie Prefab)
        {

        }

        public bool TakeDamage(int damage)
        {
            if(healthComponent.IsDead() == false)
            {
                blood.Play();
                healthComponent.TakeDamage(damage);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void KillZombie()
        {
            StartCoroutine(DestroyCurrent());
        }
        IEnumerator DestroyCurrent()
        {
            navMeshAgent.isStopped = true;
            anim.SetBool("Dead", true);
            blood.Stop();
            CapsuleCollider.enabled = false;
            GameController.Instance.AddMoney(15);
            StartCoroutine(GoInGround());
            yield return new WaitForSeconds(1.5f);
            StopCoroutine(GoInGround());
            this.DestroyPooled();
        }

        IEnumerator GoInGround()
        {
            navMeshAgent.enabled = false;
            yield return  new WaitForSeconds(1f);
            while (true)
            {
                if(transform.position.y <= -1)
                {
                    break;
                }

                var zombieTransform = transform;
                var position        = zombieTransform.position;
                position                 = new Vector3(position.x, position.y - 0.003f, position.z);
                zombieTransform.position = position;
                yield return Time.deltaTime;
            }
        }
    }
}
