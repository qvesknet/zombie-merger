﻿using CoreMechanics;
using UnityEngine;

namespace SavingAndLoading
{
    [System.Serializable]
    public class CharacterSave
    {
        public string  characterName;
        public int     level;
        public int     soIndex;
        public bool    isOnBattleGround = false;
        public bool    isInInventory    = false;
        public bool    isFound          = false;
        public Vector3 standingPos      = new Vector3(0, 0, 0);

        public void Init(Character character)
        {
            characterName = character.characterName;
            level         = character.level;
            soIndex       = character.soIndex;
            standingPos   = character.standingPos;
        }
    }
}
