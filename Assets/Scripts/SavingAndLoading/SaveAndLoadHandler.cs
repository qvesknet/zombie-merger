﻿using System.Collections.Generic;
using CoreMechanics;
using Patik.CodeArchitecture.Patterns;

// #if !UNITY_EDITOR
// [SerializationInstructions(Password ="Zoro")]
// #endif

namespace SavingAndLoading
{
    public class SaveAndLoadHandler : SingletonFromFile<SaveAndLoadHandler>
    {
        public GeneralGameSaveData data;

        protected override void OnAfterReadFromFile(SaveAndLoadHandler inst)
        {

            if (data == null)
            {
                data = new GeneralGameSaveData() { itemList = new List<CharacterSave>(), money = 500000, price = 75 };
            }
        }

        public void FillCharacterSaveFiles(Character newCharacter)
        {
            var charSave = new CharacterSave();
            charSave.isFound           = true;
            newCharacter.characterSave = charSave;
            charSave.Init(newCharacter);
            data.itemList.Add(charSave);
            Save();
        }

        public void SaveCharacterNewPosition(Character character)
        {
            foreach (var item in data.itemList)
            {
                if(item == character.characterSave)
                {
                    item.standingPos        = character.standingPos;
                    character.characterSave = item;
                }
            }
            Save();
        }

        public void UpgradeCharacterSave(Character character)
        {
            for (int i = 0; i < data.itemList.Count; i++)
            {
                if(data.itemList[i] == character.characterSave)
                {
                    data.itemList[i].level  = character.level;
                    character.characterSave = data.itemList[i];
                    break;
                }
            }
        }

        public void SaveCharacter(Character character)
        {
            if (character == null)
                return;

            for (int i = 0; i < data.itemList.Count; i++)
            {
                if(data.itemList[i] == character.characterSave)
                {
                    data.itemList[i].level            = character.level;
                    data.itemList[i].isOnBattleGround = character.isOnBattleGround;
                    data.itemList[i].isInInventory    = character.isInInventory;
                    character.characterSave           = data.itemList[i];
                }
            }
            Save();
        }

        public void RemoveFromCharacterSaveFiles(Character character)
        {
            CharacterSave save = null;
            for (int i = 0; i < data.itemList.Count; i++)
            {
                if(data.itemList[i] == character.characterSave)
                {
                    save = data.itemList[i];
                    break;
                }
            }
            data.itemList.Remove(save);
        }
    }
}
