﻿using System.Collections.Generic;

namespace SavingAndLoading
{
    [System.Serializable]
    public class GeneralGameSaveData
    {
        public int   level = 1;
        public float money;
        public int   gems;
        public float price;

        public List<CharacterSave> itemList = new List<CharacterSave>();

        public GeneralGameSaveData()
        {
            level = 1;
            money = 15000;
            gems  = 5000;
            price = 1500;
        }
    }
}
