﻿using UnityEngine;

namespace mainUI
{
    public class PauseBattle : MonoBehaviour
    {
        [SerializeField] private GameObject pausePanel;
        void Start()
        {
            pausePanel.SetActive(false);
        }
        
        public void PauseGame()
        {
            Time.timeScale = 0;
            pausePanel.SetActive(true);
            //Disable scripts that still work while timescale is set to 0
        } 
        public void ContinueGame()
        {
            Time.timeScale = 1;
            pausePanel.SetActive(false);
            //enable the scripts again
        }
    }
}