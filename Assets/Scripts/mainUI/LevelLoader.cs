﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace mainUI
{
    public class LevelLoader : MonoBehaviour
    {
        public void LoadScene(string scene)
        {
            SceneManager.LoadScene(scene);
        }
    }
}
