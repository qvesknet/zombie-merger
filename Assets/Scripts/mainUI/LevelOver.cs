﻿using System;
using CoreMechanics;
using SavingAndLoading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace mainUI
{
    public class LevelOver : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI rewardAmountText;
        [SerializeField] private Image victory;
        [SerializeField] private Image defeat;
        private int CountSoldiers;
        private int CountEnemy;

        private void OnEnable()
        {
            var rewardAmount = GameController.Instance.battleFarmMoney;
            rewardAmountText.text = rewardAmount.ToString();
            CountSoldiers = GameController.Instance.countSoldiers;
            CountEnemy = GameController.Instance.enemyCount;

            if (CountSoldiers > CountEnemy)
            {
                victory.gameObject.SetActive(true);
                defeat.gameObject.SetActive(false);
                SaveAndLoadHandler.Instance.data.level += 1;
                SaveAndLoadHandler.Save();
            }
            else
            {
                victory.gameObject.SetActive(false);
                defeat.gameObject.SetActive(true);
            }
            GameController.Instance.AddMoney(rewardAmount);
        }
    }
}