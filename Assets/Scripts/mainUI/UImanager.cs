﻿using CoreMechanics;
using InventorySystem;
using SavingAndLoading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace mainUI
{
    public class UImanager : MonoBehaviour
    {
        public static UImanager Instance;

        private void Awake()
        {
            Instance = this;
        }

        [SerializeField] Button buyButton;
        [SerializeField] Button battleButton;
        [SerializeField] Button Inventory;

        [SerializeField] TextMeshProUGUI moneyText;
        [SerializeField] Text            priceText;
        [SerializeField] TextMeshProUGUI battleMoney;

        [SerializeField] TextMeshProUGUI enemyCount;
        [SerializeField] TextMeshProUGUI playerCount;

        [SerializeField]  private GameObject      upperMainHUD;
        [SerializeField]  private GameObject      inAttackHUD;
        [SerializeField]  public  InventoryUI     inventoryUI;
        [SerializeField]  private GameObject      battleSceneButtons;
        [SerializeField]  private GameObject      gameOverPopUp;
        [HideInInspector] public  int             currentMoney;
        [SerializeField]  private TextMeshProUGUI battleLevelText;
        private                   RectTransform   moneyTextRect;

        private void Start()
        {
            inventoryUI.SetInventory(GameController.Instance.inventory);
            battleLevelText.text = "LEVEL " + SaveAndLoadHandler.Instance.data.level.ToString();
            moneyTextRect        = moneyText.GetComponent<RectTransform>();
        }

        public void Update()
        {
            if (currentMoney > 1000)
            {
                moneyText.text     = currentMoney.ToString("N0") + "K";
                moneyText.fontSize = 30;
                var anchoredPosition = moneyTextRect.anchoredPosition;
                anchoredPosition               = new Vector3(15, anchoredPosition.y);
                moneyTextRect.anchoredPosition = anchoredPosition;
            }
            else
            {
                moneyText.text     = currentMoney.ToString();
                moneyText.fontSize = 40;
                var anchoredPosition = moneyTextRect.anchoredPosition;
                anchoredPosition               = new Vector3(40, anchoredPosition.y);
                moneyTextRect.anchoredPosition = anchoredPosition;
            }
        }

        public void ChangeToBattleUI(bool set)
        {
            inAttackHUD.SetActive(set);
            upperMainHUD.SetActive(!set);
            battleSceneButtons.SetActive(!set);
        }

        public void SetPlayerAndEnemyCount(int player, int enemy)
        {
            enemyCount.text  = enemy.ToString();
            playerCount.text = player.ToString();

            if ((player <= 0 || enemy <= 0) && inAttackHUD.activeInHierarchy)
            {
                gameOverPopUp.gameObject.SetActive(true);
            }
        }
    

        public void SetBattleReward(int price)
        {
            battleMoney.text = price.ToString();
        }

        public void SetPrice(int price)
        {
            priceText.text = price.ToString();
        }

        public void DealMoney(int money)
        {
            currentMoney = money;
        }
    }
}