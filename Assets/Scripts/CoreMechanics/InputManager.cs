﻿using InventorySystem;
using mainUI;
using UnityEngine;

namespace CoreMechanics
{
    public class InputManager : MonoBehaviour
    {
        public static InputManager Instance;

        private void Awake()
        {
            Instance = this;
        }

        //public fields
        public LayerMask playerLayerMask;
        public LayerMask groundLayerMask;
        public LayerMask pointLayerMask;

        //private fields
        Touch     touch;
        Character selectedObject;

        public Character SetSelectedObj
        {
            set => selectedObject = value;
        }

        Vector3 initialPosition;

        private bool insideInventoryBox;
        private bool isSelected;

        private void LateUpdate()
        {
            if (Input.touchCount > 0)
            {
                touch = Input.GetTouch(0);
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                switch (touch.phase)
                {
                    case TouchPhase.Began:

                        if (Physics.Raycast(ray, out var hit, 100, playerLayerMask))
                        {
                            selectedObject = hit.transform.gameObject.GetComponent<Character>();
                            //selectedObject.GetComponent<Animator>().SetFloat("Hang", 1);
                            if (selectedObject != null)
                            {
                                var selectedObjectTransform = selectedObject.transform;
                                initialPosition = selectedObjectTransform.position;
                                selectedObjectTransform.position = new Vector3(
                                    initialPosition.x,
                                    initialPosition.y + 1.5f,
                                    initialPosition.z);
                                isSelected = true;
                                selectedObject.ShowLevelText(true);
                                if(GameController.Instance.inventoryIsOpen)
                                    ScrollManager.Instance.SetFakeCard(selectedObject.characterName, selectedObject.sprite, selectedObject.level);
                            }
                        }

                        break;

                    case TouchPhase.Moved:
                        if (GameController.Instance.inventoryIsOpen == true)
                        {
                            if (selectedObject != null)
                            {
                                var       selectedObjPosOnCanvas = Camera.main.WorldToScreenPoint(selectedObject.transform.position);
                                var       inventoryBox           = UImanager.Instance.inventoryUI.inventoryBox;
                                Vector3[] v                      = new Vector3[4];
                                inventoryBox.GetWorldCorners(v);
                                float corner1 = 0;
                                for (var i = 0; i < 4; i++)
                                {
                                    if (i == 1)
                                        corner1 = v[i].y;
                                }

                                if (selectedObjPosOnCanvas.y < corner1)
                                {
                                    ShowOrHideCardAndCharacter(true);
                                }
                                else
                                {
                                    ShowOrHideCardAndCharacter(false);
                                }
                            }
                        }

                        if (Physics.Raycast(ray, out var groundHit, 100, groundLayerMask))
                        {
                            if (selectedObject != null)
                            {
                                GameController.Instance.inventory.CompareItems(selectedObject.inventoryItem);
                                var selectedTransform = selectedObject.transform;
                                selectedTransform.position = new Vector3(
                                    groundHit.point.x,
                                    selectedTransform.position.y,
                                    groundHit.point.z);
                            }
                        }
                        break;

                    case TouchPhase.Ended:
                        if (selectedObject != null)
                        {
                            //selectedObject.GetComponent<Animator>().SetFloat("Hang", 0);
                            if (Physics.Raycast(ray, out var standingPointHit, 100, pointLayerMask))
                            {
                                var standingPoint = standingPointHit.transform.gameObject.GetComponent<StandingPoint>();

                                if (!standingPoint.isOccupied)
                                {
                                    var position = standingPoint.transform.position;
                                    selectedObject.transform.position = new Vector3(
                                        position.x,
                                        initialPosition.y,
                                        position.z
                                    );
                                    selectedObject.PlaceOnBattleGround();
                                }
                                else if (selectedObject.CanMerge() && standingPoint.isOccupied)
                                {
                                    // do nothing yet
                                }
                                else
                                {
                                    selectedObject.transform.position = initialPosition;
                                }
                            }
                            else
                            {
                                selectedObject.transform.position = initialPosition;
                                if (GameController.Instance.inventoryIsOpen == true)
                                {
                                    if (selectedObject.isInInventory == true)
                                    {
                                        selectedObject.inventoryCard.AddToBattleInventory();
                                        // selectedObject.gameObject.SetActive(false);
                                    }
                                }
                            }
                            selectedObject.ShowLevelText(false);
                            selectedObject  = null;
                            initialPosition = Vector3.zero;
                            isSelected      = false;
                            GameController.Instance.inventory.StopComparingItems();
                        }
                        break;
                }
            }
        }

        private void ShowOrHideCardAndCharacter(bool hide)
        {
            selectedObject.gameObject.SetActive(!hide);
            ScrollManager.Instance.fakeCard.gameObject.SetActive(hide);
            selectedObject.isInInventory = hide;
            insideInventoryBox           = hide;
        }

        public void SelectCharacter(Character selected)
        {
            selectedObject = selected;
            if (GameController.Instance.inventoryIsOpen && isSelected == false && selectedObject != null)
            {
                var selectedTransform = selectedObject.transform;
                initialPosition = selectedTransform.position;
                selectedTransform.position = new Vector3(
                    initialPosition.x - 20,
                    initialPosition.y + 1.5f,
                    initialPosition.z);
                isSelected = true;
            }
        }
    }
}