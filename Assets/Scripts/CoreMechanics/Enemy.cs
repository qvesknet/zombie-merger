﻿using UnityEngine;

namespace CoreMechanics
{
    public class Enemy : MonoBehaviour
    {
        public string enemyName;
        public float  rewardMax;
        public float  rewardMin;
    
        void Start()
        {
            GetComponent<Animator>().SetFloat("Win", 0);
            GameController.Instance.enemyCount += 1;
        }
    
        private void OnDisable()
        {
            GameController.Instance.enemyCount -= 1;
            var randomNum = Random.Range(rewardMin, rewardMax);
            var reward    = Mathf.RoundToInt(randomNum);
            GameController.Instance.battleFarmMoney += reward;
        }
    }
}
