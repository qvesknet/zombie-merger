﻿using System;
using System.Collections.Generic;
using CameraScripts;
using InventorySystem;
using mainUI;
using SavingAndLoading;
using StateMachine;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoreMechanics
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance;
        public        Inventory      inventory;

        private void Awake()
        {
            Instance  = this;
            inventory = new Inventory();
        }

        [SerializeField] Transform             standingPoints;
        [SerializeField] GameObject            playerPrefab;
        [SerializeField] CharacterScriptable[] scriptables;

        public CharacterScriptable[] GetScriptables => scriptables;

        List<Transform>        freeStandingPoints = new List<Transform>();
        public List<Transform> GetFreeStandingPoints => freeStandingPoints;

        public LayerMask playerLayerMask;
        public LayerMask enemyLayerMask;

        float price = 0;

        [SerializeField] float money           = 0;
        public           int   battleFarmMoney = 0;

        public                   bool inventoryIsOpen = false;
        [HideInInspector] public int  countSoldiers   = 0;

        [HideInInspector] public int enemyCount = 0;

        private void Start()
        {
            if (SceneManager.GetActiveScene().name != "StartingScreen")
            {
                foreach (var item in SaveAndLoadHandler.Instance.data.itemList)
                {
                    AddCharacter(item);
                }

                InventoryUI.Instance.AddExistingItem();
            }

            price = SaveAndLoadHandler.Instance.data.price;
            money = SaveAndLoadHandler.Instance.data.money;
            UImanager.Instance.DealMoney(Convert.ToInt32(money));

            CountMyCharacters();
        }

        private void CountMyCharacters()
        {
            countSoldiers = 0;
            foreach (var item in inventory.GetItemList())
            {
                if (item.GetCurrentCharacter.isOnBattleGround)
                {
                    countSoldiers++;
                }
            }
        }

        private void Update()
        {
            UImanager.Instance.DealMoney(Convert.ToInt32(money));
            UImanager.Instance.SetPrice(Convert.ToInt32(price));
            UImanager.Instance.SetBattleReward(battleFarmMoney);
            UImanager.Instance.SetPlayerAndEnemyCount(countSoldiers, enemyCount);
        }

        public void ToBattle()
        {
            CountMyCharacters();
            if (countSoldiers > 0 && enemyCount > 0)
            {
                UImanager.Instance.ChangeToBattleUI(true);
                CameraPositioning.Instance.changeToAttack = true;
                standingPoints.gameObject.SetActive(false);
                var position = transform.position;
                var army     = Physics.OverlapSphere(position, 100f, playerLayerMask);
                var enemies  = Physics.OverlapSphere(position, 100f, enemyLayerMask);
                GetComponent<InputManager>().enabled = false;

                foreach (var soldier in army)
                {
                    var unit = soldier.GetComponent<Unit>();
                    unit.stateMachine.ChangeState(unit.move);
                }

                foreach (var enemy in enemies)
                {
                    var unit = enemy.GetComponent<Unit>();
                    unit.stateMachine.ChangeState(unit.move);
                }
            }
        }

        public void AddCharacter(CharacterSave character)
        {
            var playerClone          = Instantiate(playerPrefab, character.standingPos, Quaternion.identity);
            var playerCloneCharacter = playerClone.GetComponent<Character>();
            playerCloneCharacter.characterSave = character;
            var charVersion = scriptables[character.soIndex];
            playerCloneCharacter.GiveLife(charVersion, character.soIndex);
            playerCloneCharacter.level                  = character.level;
            playerClone.GetComponent<Animator>().avatar = charVersion.avatar;
            playerCloneCharacter.inventoryItem.SetCharacter(playerCloneCharacter);
            inventory.AddItem(playerCloneCharacter.inventoryItem);
        }

        public void BuyCharacter()
        {
            if (price > money)
            {
                return;
            }

            SearchFreeStandingPoint();

            if (freeStandingPoints.Count > 0)
            {
                var freePoint = freeStandingPoints[0];
                freePoint.GetComponent<StandingPoint>().isOccupied = true;
                var position       = freePoint.position;
                var playerPosition = new Vector3(position.x, 1.015f, position.z);
                var playerClone    = Instantiate(playerPrefab, playerPosition, Quaternion.identity);
                var randomNum      = UnityEngine.Random.Range(0, scriptables.Length);
                var charVersion    = scriptables[randomNum];
                playerClone.GetComponent<Animator>().avatar = charVersion.avatar;
                var playerCloneCharacter = playerClone.GetComponent<Character>();
                playerCloneCharacter.level = 1;
                // playerCloneCharacter.levelText.text = "1";
                playerCloneCharacter.GiveLife(charVersion, randomNum);
                playerCloneCharacter.isOnBattleGround = true;
                playerCloneCharacter.PlaceOnBattleGround();
                playerCloneCharacter.inventoryItem.SetCharacter(playerCloneCharacter);
                inventory.AddItem(playerCloneCharacter.inventoryItem);
                InventoryUI.Instance.CreateCard(InventoryUI.Instance.transform, playerCloneCharacter.inventoryItem, false);
                SaveAndLoadHandler.Instance.FillCharacterSaveFiles(playerCloneCharacter);
                money                                  -= price;
                SaveAndLoadHandler.Instance.data.money =  money;
                SaveAndLoadHandler.Save();
            }
            else return;
        }

        public void SearchFreeStandingPoint()
        {
            freeStandingPoints.Clear();

            for (int i = 0; i < standingPoints.childCount; i++)
            {
                var point         = standingPoints.GetChild(i);
                var standingPoint = point.GetComponent<StandingPoint>();
                if (standingPoint.isOccupied == false)
                {
                    freeStandingPoints.Add(point);
                }
            }
        }

        public void AddMoney(float amount)
        {
            money                                  += amount;
            SaveAndLoadHandler.Instance.data.money =  money;
            SaveAndLoadHandler.Save();
        }
    }
}