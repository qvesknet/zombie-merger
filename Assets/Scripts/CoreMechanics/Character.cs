﻿using InventorySystem;
using SavingAndLoading;
using StateMachine;
using TMPro;
using UnityEngine;

namespace CoreMechanics
{
    public class Character : MonoBehaviour
    {
        public string characterName;
        public int    level = 1;

        public LayerMask      playerLayerMask;
        public ParticleSystem mergeParticle;
        public ParticleSystem groundingParticle;
        public ParticleSystem mergeMatchParticle;
        public TextMeshPro    levelText;
        public int            soIndex;

        GameObject                             other;
        public                   bool          isOnBattleGround = false;
        public                   bool          isInInventory    = false;
        public                   InventoryCard inventoryCard;
        [HideInInspector] public InventoryItem inventoryItem;
        [HideInInspector] public Sprite        sprite        = null;
        [HideInInspector] public Vector3       standingPos   = new Vector3(0, 0, 0);
        [HideInInspector] public CharacterSave characterSave = null;

        private HealthComponent healthComponent;

        private void Awake()
        {
            inventoryItem   = new InventoryItem();
            healthComponent = GetComponent<HealthComponent>();
        }

        private void Start()
        {
            MatchingWithOtherCharacter(false);
            levelText.text = level.ToString();
        }

        public void GiveLife(CharacterScriptable scriptableObject, int id)
        {
            sprite  = scriptableObject.sprite;
            soIndex = id;
            var body     = Instantiate(scriptableObject.body, transform);
            var position = body.transform.position;
            position                = new Vector3(position.x, 0, position.z);
            body.transform.position = position;

            characterName = scriptableObject.characterName;
            healthComponent.SetHealth(scriptableObject.health);
            levelText.text   = characterSave.level.ToString();
            isOnBattleGround = characterSave.isOnBattleGround;
            isInInventory    = characterSave.isInInventory;

            if (isInInventory == true)
            {
                gameObject.SetActive(false);
            }
            else
            {
                PlaceOnBattleGround();
            }
        }

        public void ShowLevelText(bool show)
        {
            levelText.gameObject.SetActive(show);
        }

        public bool CanMerge()
        {
            var unit = GetComponent<Unit>();
            if (unit != null && unit.stateMachine.CurrentState == unit.idle)
            {
                other = null;
                var   overlap = Physics.OverlapSphere(transform.position, 1.5f, playerLayerMask);
                float distance;
                float nearestDistance = float.MaxValue;
                if (overlap.Length > 1)
                {
                    foreach (var obj in overlap)
                    {
                        if (obj.gameObject != this.gameObject)
                        {
                            distance = (transform.position - obj.transform.position).sqrMagnitude;
                            if (distance < nearestDistance)
                            {
                                nearestDistance = distance;
                                other           = obj.gameObject;
                            }
                        }
                    }

                    if (other != null)
                    {
                        var otherCharacter = other.GetComponent<Character>();
                        if (otherCharacter.level == level && otherCharacter.characterName == characterName)
                        {
                            UpgradeCharacter();
                            transform.position = other.transform.position;
                            levelText.text     = level.ToString();
                            GameController.Instance.inventory.RemoveItem(otherCharacter.inventoryItem);
                            mergeParticle.Play();
                            Destroy(otherCharacter.inventoryCard.gameObject);
                            Destroy(other);

                            SaveAndLoadHandler.Instance.RemoveFromCharacterSaveFiles(otherCharacter);
                            SaveAndLoadHandler.Save();
                            PlaceOnBattleGround();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public void UpgradeCharacter()
        {
            level++;
            healthComponent.UpgradeHealth();
            SaveAndLoadHandler.Instance.UpgradeCharacterSave(this);
            inventoryItem.SetCharacter(this);
            inventoryCard.SetInventoryItem(inventoryItem);
        }

        public void PlaceOnBattleGround()
        {
            MatchingWithOtherCharacter(false);
            isInInventory    = false;
            isOnBattleGround = true;
            standingPos      = transform.position;
            inventoryCard.RemoveFromBattleInventory();
            gameObject.SetActive(true);
            SaveAndLoadHandler.Instance.SaveCharacterNewPosition(this);
            groundingParticle.Play();
        }

        public void MatchingWithOtherCharacter(bool match)
        {
            mergeMatchParticle.gameObject.SetActive(match);
        }

        private void OnDestroy()
        {
            SaveAndLoadHandler.Instance.SaveCharacter(this);
        }

        private void OnDisable()
        {
            isOnBattleGround = false;
            if (GameController.Instance.countSoldiers > 0)
                GameController.Instance.countSoldiers -= 1;
        }
    }
}