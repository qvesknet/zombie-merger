﻿using UnityEngine;

namespace CoreMechanics
{
    public class StandingPoint : MonoBehaviour
    {
        public bool isOccupied = false;
        Material    mat;
        Color       initialColor;

        [HideInInspector] public GameObject occupierObject = null;

        public LayerMask playerLayerMask;
        Ray              ray;
        private void Start()
        {
            mat          = GetComponent<Renderer>().material;
            initialColor = mat.color;
            ray          = new Ray(transform.position, Vector3.up);
        }

        private void Update()
        {
            CheckHover();
            CheckIfFree();
        }

        public void CheckIfFree()
        {
            var overlap = Physics.OverlapSphere(transform.position, 1f, playerLayerMask);
            if (overlap.Length > 0)
            {
                occupierObject = overlap[0].gameObject;
                isOccupied     = true;
                mat.color      = new Color32(255, 122, 232, 55);
            }
            else
            {
                occupierObject = null;
                isOccupied     = false;
            }
        }

        private void CheckHover()
        {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 3f, playerLayerMask))
            {
                mat.color = new Color32(122, 160, 255, 100);
            }
            else
            {
                mat.color = initialColor;
            }
        }
    }
}
