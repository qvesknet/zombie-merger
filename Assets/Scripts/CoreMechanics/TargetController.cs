﻿using System.Collections.Generic;
using UnityEngine;

namespace CoreMechanics
{
    public class TargetController : MonoBehaviour
    {
        LayerMask _targetLayerMask;

        public LayerMask TargetLayerMask
        {
            set { _targetLayerMask = value; }
        }

        public   int            AliveTargets { get; private set; }
        readonly List<Collider> aliveTargets = new List<Collider>();

        Collider target;
        public GameObject SearchTarget(float radius)
        {
            aliveTargets.Clear();
            var colliders = Physics.OverlapSphere(transform.position, radius, _targetLayerMask);

            float distance;
            float nearestDistance = float.MaxValue;


            if(colliders.Length <= 0)
            {
                return null;
            }

            foreach (var collider in colliders)
            {
                if (!collider.GetComponent<HealthComponent>().IsDead())
                {
                    aliveTargets.Add(collider);
                    distance = (transform.position - collider.transform.position).sqrMagnitude;
                    if (distance < nearestDistance)
                    {
                        nearestDistance = distance;
                        target          = collider;
                    }
                }
            }
            AliveTargets = aliveTargets.Count;
            return target.gameObject;
        }
    }
}
