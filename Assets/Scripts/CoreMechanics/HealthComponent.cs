﻿using System.Collections;
using System.Collections.Generic;
using Patik.CodeArchitecture.Systems;
using StartScreenZombies;
using StateMachine;
using UnityEngine;
using UnityEngine.Events;

namespace CoreMechanics
{
    public interface Idamagable
    {
        void TakeDamage(int damage);
    }

    public sealed class HealthComponent : MonoBehaviour, Idamagable
    {
        [SerializeField] private int health;
        private                  int initialHealth;
        Unit                         unit;

        public void SetHealth(int amount)
        {
            health        = amount;
            initialHealth = health;
        }

        private void Start()
        {
            unit = GetComponent<Unit>();
        }

        public void TakeDamage(int damage)
        {
            health -= damage;
        }

        public bool IsDead()
        {
            if(health <= 0)
            {
                if (!GetComponent<Zombie>())
                    unit.stateMachine.ChangeState(unit.death);
                return true;
            }
            return false;
        }

        public void ResetHealth()
        {
            health = initialHealth;
        }

        public void UpgradeHealth()
        {
            health *= 2;
        }
    }
}