﻿using System;
using UnityEngine;

namespace CameraScripts
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField] private Vector2 clampedXZmin;
        [SerializeField] private Vector2 clampedXZmax;
        private float clampedY;
        [SerializeField] [Range(0, 1)] private float movementSpeed = 1f;
        private void OnEnable()
        {
            clampedY = transform.position.y;
        }

        private void Update()
        {
            MoveCamera();
        }

        private void MoveCamera()
        {

            if (Input.touchCount        > 0 &&
                Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                // Get movement of the finger since last frame
                var touchDeltaPosition = Input.GetTouch(0).deltaPosition * (movementSpeed * Time.deltaTime);

                // Move object across XY plane
                transform.Translate(-touchDeltaPosition.x , 0, -touchDeltaPosition.y) ;

                var clampedX = Mathf.Clamp(transform.position.x, clampedXZmin.x, clampedXZmax.x);
                var clampedZ = Mathf.Clamp(transform.position.z, clampedXZmin.y, clampedXZmax.y);
                transform.position = new Vector3(clampedX, clampedY, clampedZ);
            }
        }
    }
}