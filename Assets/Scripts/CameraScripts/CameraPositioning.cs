﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace CameraScripts
{
    public class CameraPositioning : MonoBehaviour
    {
        public static CameraPositioning Instance;

        private void Awake()
        {
            Instance = this;
        }

        private                  Vector3 cameraStartingPos;
        private                  Vector3 cameraStartingRot;
        public                   bool    changeToAttack;
        [FormerlySerializedAs("cameraSpeed")] [SerializeField] private float   rotationSpeed = 10f;

        private                  Vector2 worldStartPoint;
        private CameraMovement cameraMovement;
        private void Start()
        {
            cameraMovement = GetComponent<CameraMovement>();
            cameraMovement.enabled = false;
        }

        private void OnEnable()
        {
            cameraStartingPos = transform.position;
            cameraStartingRot = transform.localEulerAngles;
            changeToAttack    = false;
        }

        private void Update()
        {
            if (changeToAttack)
            {
                ChangeToAttack();
            }
        }

        

        public void ChangeToInventoryUI()
        {
            transform.position         = new Vector3(0,  17, -4);
            transform.localEulerAngles = new Vector3(80, 0,  0);
        }

        public void ChangeToBattleGround()
        {
            transform.position         = cameraStartingPos;
            transform.localEulerAngles = cameraStartingRot;
        }

        private void ChangeToAttack()
        {
            var attackPos = new Vector3(6,   17,   9);
            var rotation  = new Vector3(65f, -65f, 0);

            // es updateshi unda gavaketo
            transform.position         = Vector3.MoveTowards(transform.position,         attackPos, rotationSpeed     * Time.deltaTime);
            transform.localEulerAngles = Vector3.MoveTowards(transform.localEulerAngles, rotation,  rotationSpeed * 4 * Time.deltaTime);

            if (transform.position == attackPos)
            {
                cameraMovement.enabled = true;
                changeToAttack = false;
            }
        }
    }
}