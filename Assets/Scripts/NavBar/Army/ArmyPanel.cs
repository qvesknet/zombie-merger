﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using CoreMechanics;
using InventorySystem;
using SavingAndLoading;

public class ArmyPanel : MonoBehaviour
{
    [SerializeField] RectTransform locked;
    [SerializeField] RectTransform found;
    [SerializeField] RectTransform slot;

    private Dictionary<string, int> foundCharacterNamesAndIndex = new Dictionary<string, int>();
    private void Start()
    {
        AddFoundCharacters();
        AddLockedCharacters();
    }
    private void AddFoundCharacters()
    {
        for (int i = 0; i < SaveAndLoadHandler.Instance.data.itemList.Count; i++)
        {
            if (SaveAndLoadHandler.Instance.data.itemList[i].isFound == true)
            {
                var characterName = SaveAndLoadHandler.Instance.data.itemList[i].characterName;
                var characterIndex = SaveAndLoadHandler.Instance.data.itemList[i].soIndex;
                if (!foundCharacterNamesAndIndex.ContainsKey(characterName))
                {
                    foundCharacterNamesAndIndex.Add(characterName, characterIndex);
                }

            }
        }
        for (int i = 0; i < foundCharacterNamesAndIndex.Count; i++)
        {
            var card = Instantiate(slot, found);
            card.SetParent(found, false);
            var item = foundCharacterNamesAndIndex.ElementAt(i);
            var sprite = GameController.Instance.GetScriptables[item.Value].sprite;
            var nameOfCharacter = item.Key;
            var inventoryCard = card.GetComponent<InventoryCard>();
            inventoryCard.SetCardNameAndPhoto(nameOfCharacter, sprite);
            inventoryCard.enabled = false;
            inventoryCard.HideLevelInArmyPanel();
        }
    }

    private void AddLockedCharacters()
    {
        var characterScriptables = GameController.Instance.GetScriptables;
        var foundChars = SaveAndLoadHandler.Instance.data.itemList;
        for (int i = 0; i < characterScriptables.Length; i++)
        {
            if(foundChars.Any(c => c.soIndex == i))
            {
                continue;
            }
            var card = Instantiate(slot, found);
            card.SetParent(locked, false);
            var inventoryCard = card.GetComponent<InventoryCard>();
            var cardCanvasGroup = card.GetComponent<CanvasGroup>();
            cardCanvasGroup.alpha = 0.5f;
            inventoryCard.SetCardNameAndPhoto(characterScriptables[i].characterName, characterScriptables[i].sprite);
            inventoryCard.enabled = false;
            inventoryCard.HideLevelInArmyPanel();

        }
    }
}
