﻿using UnityEngine;
using UnityEngine.UI;

namespace NavBar
{
    public class NavBarToggleButtonDown : MonoBehaviour
    {
        private Toggle toggle;
        private void Start()
        {
            toggle = GetComponent<Toggle>();
        }

        public void OnButtonDown()
        {
            toggle.isOn = true;
        }
    }
}