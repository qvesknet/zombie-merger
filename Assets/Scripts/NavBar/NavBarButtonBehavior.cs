﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class NavBarButtonBehavior : MonoBehaviour
{
    Toggle toggle;
    Transform text;
    Transform icon;
    Transform icon_off;
    Transform background;
    [SerializeField] GameObject panel;
    private void Start()
    {
        toggle = gameObject.GetComponent<Toggle>();
        background = this.transform.Find("Background");
        text = background.transform.Find("Text");
        icon = background.transform.Find("Icon");
        icon_off = background.transform.Find("Icon_Off");
       
        toggle.onValueChanged.AddListener(delegate {
            OnToggle();
        });
        
        if(toggle.name == "Home")
        {
            toggle.isOn = false;

            toggle.isOn = true;
        }
    }
    public void OnToggle()
    {
        if (toggle.isOn)
        {
            text.gameObject.SetActive(true);
            icon_off.gameObject.SetActive(false);
            icon.gameObject.SetActive(true);
            icon.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 70);
            if(panel != null)
                panel.SetActive(true);
        }
        else if (!toggle.isOn)
        {
            text.gameObject.SetActive(false);
            icon_off.gameObject.SetActive(true);
            icon.gameObject.SetActive(false);
            icon.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            if (panel != null)
                panel.SetActive(false);
        }
    }
}
