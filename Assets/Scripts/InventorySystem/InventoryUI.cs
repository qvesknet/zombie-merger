﻿using System.Collections.Generic;
using CameraScripts;
using CoreMechanics;
using UnityEngine;

namespace InventorySystem
{
    public class InventoryUI : MonoBehaviour
    {
        public static InventoryUI Instance;

        Vector3 cameraStartingPos;
        Vector3 cameraStartingRot;

        private Inventory     _inventory;
        public  RectTransform container;
        public  RectTransform inventoryBox;
        public  RectTransform slot;

        [HideInInspector]public Canvas Canvas;
        private void Awake()
        {
            Instance = this;
            Canvas = GetComponent<Canvas>();
        }
        
        public List<RectTransform> cardList = new List<RectTransform>();

        public void SetInventory(Inventory inventory)
        {
            this._inventory = inventory;
        }

        public void RemoveCard(RectTransform card)
        {
            cardList.Remove(card);
        }

        public void AddAllItemSlot()
        {
            for (var i = 0; i < cardList.Count; i++)
            {
                var card = cardList[i];
                if (card != null)
                {
                    card.gameObject.SetActive(true);
                    card.SetParent(container);
                }
            }
            _inventory.RecallArmy();
        }

        public void ActivateSelf()
        {
            GameController.Instance.inventoryIsOpen = true;
            CameraPositioning.Instance.ChangeToInventoryUI();
            inventoryBox.gameObject.SetActive(true);
        }

        public void AddExistingItem()
        {
            for (int i = 0; i < GameController.Instance.inventory.GetItemList().Count; i++)
            {
                var ownCharacter = GameController.Instance.inventory.GetItemList()[i].GetCurrentCharacter;
                switch (ownCharacter.isInInventory)
                {
                    case true:
                    {
                        var item = GameController.Instance.inventory.GetItemList()[i];
                        CreateCard(container, item, true);
                        break;
                    }

                    case false when ownCharacter.isOnBattleGround:
                    {
                        var item = GameController.Instance.inventory.GetItemList()[i];
                        CreateCard(transform, item, false);
                        break;
                    }
                }
            }
        }

        public void CreateCard(Transform parentTransform, InventoryItem item, bool active)
        {
            var card = Instantiate(slot, parentTransform);
            var inventoryCard = card.GetComponent<InventoryCard>();
            inventoryCard.SetInventoryItem(item);
            inventoryCard.GetCharacter.inventoryCard = inventoryCard;
            card.SetParent(parentTransform, false);
            card.gameObject.SetActive(active);
            if (!cardList.Contains(card))
                cardList.Add(card);
        }
        
        
        public void HideSelf()
        {
            GameController.Instance.inventoryIsOpen = false;
            CameraPositioning.Instance.ChangeToBattleGround();
            inventoryBox.gameObject.SetActive(false);
        }
    }
}
