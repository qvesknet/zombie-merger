﻿using CoreMechanics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace InventorySystem
{
    public class InventoryCard : MonoBehaviour
    {
        public Image           image;
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI level;
        public GameObject LevelStar;
        InventoryItem          currentInventoryItem;
        CanvasGroup            canvasGroup;
        RectTransform          rectTransform;
        public RectTransform GetRectTransform => rectTransform;
        Character              character;

        bool startDragCount = false;

        public Character GetCharacter
        {
            get { return character; }
        }

        public Character SetCharacter
        {
            set { character = value; }
        }

        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            canvasGroup   = GetComponent<CanvasGroup>();

            if (character != null)
            {
                character.inventoryCard = this.GetComponent<InventoryCard>();
            }
        }

        public void PointerUpFast()
        {
            GameController.Instance.SearchFreeStandingPoint();
            Vector3 freePos = GameController.Instance.GetFreeStandingPoints[0].position;
            GetCharacter.gameObject.SetActive(true);
            GetCharacter.transform.position = new Vector3(freePos.x, 1, freePos.z);
            GetCharacter.PlaceOnBattleGround();
            gameObject.SetActive(false);
        }

        public void DragCard()
        {
            canvasGroup.alpha        = 0.5f;
            canvasGroup.interactable = false;
            InputManager.Instance.SelectCharacter(GetCharacter);
        }

        public void DragEnd()
        {
            canvasGroup.alpha        = 1f;
            canvasGroup.interactable = true;
            if (gameObject.transform.parent == InventoryUI.Instance.container)
            {
                GetCharacter.gameObject.SetActive(false);
            }
        }

        public void RemoveFromBattleInventory()
        {
            if (gameObject.transform.parent == InventoryUI.Instance.container)
            {
                gameObject.transform.SetParent(transform.parent.parent.parent.parent);
                gameObject.SetActive(false);
            }
        }

        public void AddToBattleInventory()
        {
            gameObject.SetActive(true);
            if (gameObject.transform.parent == InventoryUI.Instance.Canvas.transform)
                gameObject.transform.SetParent(InventoryUI.Instance.container);
        }

        public void HideLevelInArmyPanel()
        {
            LevelStar.SetActive(false);
        }
        
        public void SetCardNameAndPhoto(string characterName, Sprite img)
        {
            nameText.text = characterName;
            image.sprite  = img;
        }

        public void SetInventoryItem(InventoryItem item)
        {
            currentInventoryItem = item;
            nameText.text        = currentInventoryItem.CharacterName;
            character            = currentInventoryItem.GetCurrentCharacter;
            image.sprite         = character.sprite;
            level.text           = character.level.ToString();
        }
    }
}