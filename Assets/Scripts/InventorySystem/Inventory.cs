﻿using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem
{
    [System.Serializable]
    public class Inventory 
    {
        public List<InventoryItem> itemList = new List<InventoryItem>();

        public void AddItem(InventoryItem item)
        {
            itemList.Add(item);
        }

        public void RemoveItem(InventoryItem item)
        {
            itemList.Remove(item);
            InventoryUI.Instance.RemoveCard(item.GetCurrentCharacter.inventoryCard.GetRectTransform);
        }

        public void CompareItems(InventoryItem inventoryItem)
        {
            foreach (var item in GetItemList())
            {
                if(inventoryItem != item)
                {
                    if(item.Level == inventoryItem.Level && item.CharacterName == inventoryItem.CharacterName)
                    {
                        item.GetCurrentCharacter.MatchingWithOtherCharacter(true);
                    }
                }
            }
        }
       
        public void StopComparingItems()
        {
            foreach (var item in GetItemList())
            {
                item.GetCurrentCharacter.MatchingWithOtherCharacter(false);
            }
        }

        public void RecallArmy()
        {
            foreach (var item in itemList)
            {
                if(item != null)
                {
                    item.GetCurrentCharacter.gameObject.SetActive(false);
                    item.GetCurrentCharacter.isInInventory = true;
                }
            }
        }

        public List<InventoryItem> GetItemList()
        {
            return itemList;
        }
    }
}
