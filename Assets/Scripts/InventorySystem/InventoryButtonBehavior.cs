﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace InventorySystem
{
    public class InventoryButtonBehavior : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        RectTransform            rect;
        Vector2                  size;
        [SerializeField] Vector2 clickedSize;
        private void Start()
        {
            rect = this.GetComponent<RectTransform>();
            size = rect.sizeDelta;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            rect.sizeDelta = clickedSize;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            rect.sizeDelta = size;
        }

   
    }
}
