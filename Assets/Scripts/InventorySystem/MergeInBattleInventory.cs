﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreMechanics;
using SavingAndLoading;
using UnityEngine;

namespace InventorySystem
{
    public class MergeInBattleInventory : MonoBehaviour
    {
        private List<Character> charactersInBattleInventory = new List<Character>();

        public void MergeAll()
        {
            AddOnlyInventoryExistingCharacters();
            UpgradeCharacter();
        }

        private void UpgradeCharacter()
        {
            var generalInventory = GameController.Instance.inventory;
            var ints             = GetIndexOfDuplicateCharacters();
            for (var i = 0; i < ints.Count; i++)
            {
                if (i % 2 == 0)
                {
                    var index              = ints[i];
                    var characterToUpgrade = charactersInBattleInventory[index];
                    characterToUpgrade.UpgradeCharacter();
                    characterToUpgrade.levelText.text = characterToUpgrade.level.ToString();
                }

                else
                {
                    var index             = ints[i];
                    var characterToDelete = charactersInBattleInventory[index];
                    Destroy(characterToDelete.inventoryCard.gameObject);
                    Destroy(characterToDelete.gameObject);
                    generalInventory.RemoveItem(characterToDelete.inventoryItem);
                    SaveAndLoadHandler.Instance.RemoveFromCharacterSaveFiles(characterToDelete);
                }
            }
        }

        private List<int> GetIndexOfDuplicateCharacters()
        {
            var indexOfCharacters = new List<int>();

            var query = FindAllDuplicates().GroupBy(x => x)
                                           .Where(g => g.Count() > 1)
                                           .Select(y => y.Key)
                                           .ToList();

            for (var i = 0; i < charactersInBattleInventory.Count; i++)
            {
                var character = charactersInBattleInventory[i];
                foreach (var keyValuePair in query)
                {
                    if (character.characterName == keyValuePair.Key && character.level == keyValuePair.Value)
                    {
                        indexOfCharacters.Add(i);
                    }
                }
            }

            return indexOfCharacters;
        }

        private List<KeyValuePair<string, int>> FindAllDuplicates()
        {
            var                             sameNameCharacters = FindDuplicatesWithName();
            List<KeyValuePair<string, int>> dict               = new List<KeyValuePair<string, int>>();

            foreach (var sameNameCharacter in sameNameCharacters)
            {
                foreach (var character in charactersInBattleInventory)
                {
                    if (sameNameCharacter == character.characterName)
                    {
                        dict.Add(new KeyValuePair<string, int>(character.characterName, character.level));
                    }
                }
            }

            return dict;
        }

        private List<string> FindDuplicatesWithName()
        {
            var query = charactersInBattleInventory.GroupBy(x => x.characterName)
                                                   .Where(g => g.Count() > 1)
                                                   .Select(y => y.Key)
                                                   .ToList();
            return query;
        }

        private void AddOnlyInventoryExistingCharacters()
        {
            charactersInBattleInventory.Clear();
            var allCharacters = GameController.Instance.inventory.GetItemList();
            charactersInBattleInventory = allCharacters
                                          .Where(x => x.GetCurrentCharacter.isInInventory)
                                          .Select(x => x.GetCurrentCharacter)
                                          .ToList();
        }
    }
}