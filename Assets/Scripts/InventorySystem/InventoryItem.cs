﻿using CoreMechanics;
using UnityEngine;

namespace InventorySystem
{
    [System.Serializable]
    public class InventoryItem
    {
        string _characterName;
        int    _level;
        Sprite _sprite;
        private Character currentCharacter;
        public string CharacterName => this._characterName;
        public int Level => this._level;
        public void SetCharacter(Character character)
        {
            currentCharacter = character;
            SetCharacterStats();
        }

        public Character GetCurrentCharacter => currentCharacter;

        private void SetCharacterStats()
        {
            _characterName = currentCharacter.characterName;
            _level         = currentCharacter.level;
            _sprite        = currentCharacter.sprite;
        }

       
    }
}
