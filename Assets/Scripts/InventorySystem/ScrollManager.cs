﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace InventorySystem
{
    public class ScrollManager : MonoBehaviour, IEndDragHandler, IDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        public static ScrollManager Instance;
        private void Awake()
        {
            Instance = this;
        }
        private float holdTime     = 0.05f; // or whatever //
        private float holdTimeLong = 0.5f; // or whatever //
        private float acumTime     = 0;
        bool          isSelected;
    
        InventoryCard currentCard;
        RectTransform rectTransform;

        [SerializeField] public InventoryCard fakeCard;
        ScrollRect                            scrollRect;
    
        bool isDraggingCard = false;
    
        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            scrollRect    = GetComponent<ScrollRect>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (isSelected == false)
            {
                currentCard = eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryCard>();
                if (currentCard != null)
                    SetFakeCard(currentCard.GetCharacter.characterName, currentCard.image.sprite, currentCard.GetCharacter.level);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (currentCard != null)
            {
                currentCard.DragEnd();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (isDraggingCard == false && currentCard != null && currentCard != eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryCard>())
            {
                currentCard = null;
            }
            if (isDraggingCard)
            {
                scrollRect.vertical = false;
                if (currentCard != null) currentCard.DragCard();
                fakeCard.gameObject.SetActive(true);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            fakeCard.gameObject.SetActive(false);
            scrollRect.vertical = true;
        }

        void Update()
        {
            if (Input.touchCount > 0)
            {
                var touch = Input.GetTouch(0);
                acumTime += Time.deltaTime;

                if (acumTime >= holdTimeLong)
                {
                    if (currentCard != null && isDraggingCard == false)
                    {
                        isDraggingCard = true;
                    }
                }
                HandleFakeCard(touch.position);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        if (currentCard != null)
                            isSelected = true;
                        break;

                    case TouchPhase.Ended:
                        if (acumTime >= holdTime && acumTime < holdTimeLong)
                        {
                            if (currentCard != null)
                            {
                                currentCard.PointerUpFast();
                            }
                        }

                        acumTime       = 0;
                        currentCard    = null;
                        isDraggingCard = false;
                        isSelected     = false;
                        fakeCard.gameObject.SetActive(false);
                        break;
                }
            }
        }

        public void SetFakeCard(string _name, Sprite _sprite, int level)
        {
            if(fakeCard != null)
            fakeCard.SetCardNameAndPhoto(_name, _sprite);
            fakeCard.level.text = level.ToString();
        }

        private void HandleFakeCard(Vector2 touchPos)
        {
            fakeCard.GetComponent<RectTransform>().position = touchPos;
        }
    }
}
