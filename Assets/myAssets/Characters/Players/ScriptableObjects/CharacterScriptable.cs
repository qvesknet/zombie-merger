﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "CharacterPlaceHolder", menuName = "ScriptableObjects/CharacterItem")]
public class CharacterScriptable : ScriptableObject
{
    public string characterName;
    public int level = 1;
    public GameObject body;
    public Avatar avatar;
    public int health;
    public Sprite sprite;
}
