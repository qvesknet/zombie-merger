﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[ExecuteAlways]
public class GlobalMaterialController : MonoBehaviour
{
    [Header("References")]
    public List<Material> mats;

    [Header("Parameters")] 
    [Range(0, 1)] public float shadows = 0.2f;
    [Range(0, 2)] public float saturation = 1.2f;
    [Range(0, 1)] public float light = 0.85f;
    public Color nightColor = Color.black;
    
    void Update()
    {
        foreach (Material material in mats)
        {
            SetMaterialParameters(material);
        }
    }

    [ContextMenu("Reset Parameters")]
    public void ResetParameters()
    {
        shadows = 0.2f;
        saturation = 1.2f;
        light = 0.85f;
        nightColor = Color.black;
        
        foreach (Material material in mats)
        {
            SetMaterialParameters(material);
        }
    }

    private void SetMaterialParameters(Material _mat)
    {
        _mat.SetFloat("_Shadows",    shadows);
        _mat.SetFloat("_Saturation", saturation);
        _mat.SetFloat("_Light",      light);
        _mat.SetColor("_NightColor", nightColor);
    }
}
